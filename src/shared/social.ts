export interface Social{
	id : number;
	name : string;
	type : string;
	body_setting : string;
	createdAt : string;
	updatedAt : string;

}