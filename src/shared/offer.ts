export interface Offer {
	id? : number;
	title  :string;
	body : string;
	startdate : string;
	enddate:string;
	image :string;
	regionId? : number;
	cityId? : number;

}
