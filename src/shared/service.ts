export interface Service {
	id : number;
	name : string;
	before : string;
	after : string;
}