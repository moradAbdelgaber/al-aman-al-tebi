export interface Branch{
	id?:number;
	clinics_id : number;
	name_branches : string;
	street : string;
	lat : string;
	lng : string;
	admin : boolean;
}
