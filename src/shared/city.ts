export interface City{
	id? : number;
	region_id : string;
	city_name : string;
}