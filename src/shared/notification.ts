export interface Notification {
	id? : number;
	text : string;
	image : string;
}