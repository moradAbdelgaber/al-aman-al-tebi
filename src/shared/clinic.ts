import {Branch} from './Branch';
import {Service} from './service';

export interface Clinic {
	id :number;
	user_id? : string;
	name : string;
	body : string;
	image : string;
	tages : any;
	street : string;
	phone : string;
	lat : number;
	lng : number;
	region_id : number;
	city_id : number;
	branches : Branch[];
	serveseprice : Service[];
	sprice : any;
}

