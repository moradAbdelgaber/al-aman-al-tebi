import {City} from './city';


export interface Region {
	id? : number;
	region_name : string;
	city : City[];
}