export interface Card {
	id?:number;
	name : string;
	age : number;
	nationality : string;
	phone: string;
	note?: string;
}
