import { Component, ViewChild } from '@angular/core';
import { Nav, Platform , ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowserOptions ,  InAppBrowser  } from '@ionic-native/in-app-browser';
import {DataProvider} from '../providers/data/data';
import {Social} from '../shared/social';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';

  pages: Array<{title: string, component: string , icon : string}>;

  //contact
  phone : Social;
  facebook : Social;
  youtube : Social;
  instagram : Social;
  twitter : Social;
  snapchat : Social;
  whatsapp : Social;

  constructor(public platform: Platform, public statusBar: StatusBar ,  public splashScreen: SplashScreen ,
   private iab : InAppBrowser ,private toastCtrl : ToastController , private dataService : DataProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'الرئيسية', component: 'HomePage'  , icon : 'assets/imgs/home.png'},
      { title: 'من نحن', component: 'AboutPage' , icon : 'assets/imgs/about.png' } ,
      { title: 'الدليل الطبي', component: 'DalielPage' , icon : 'assets/imgs/daliel.png' },
      { title: 'التنبيهات', component: 'NotificationsPage'  , icon : 'assets/imgs/notifications.png'},
      { title: 'العروض', component: 'OffersPage' , icon : 'assets/imgs/offers.png' },
      { title: 'طلب بطاقة', component: 'CardPage' , icon : 'assets/imgs/card.png' },
      { title: 'اتصل بنا', component: 'ContactPage' , icon : 'assets/imgs/contact.png' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      // get contact details before sidemenu opened
      this.getContact();

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  //contact Info

    getContact() {
      this.dataService.getContact()
        .subscribe(res => {
        
          let contacts : Social[] = res['data'];
         
          this.phone = contacts.filter(contact => contact.name == 'phone')[0];
          this.facebook = contacts.filter(contact => contact.name == 'Facebook')[0];
          this.youtube = contacts.filter(contact => contact.name == 'YouTube')[0];
          this.instagram = contacts.filter(contact => contact.name == 'Instagram')[0];
          this.snapchat = contacts.filter(contact => contact.name == 'SnapChat')[0];
          this.twitter = contacts.filter(contact => contact.name == 'Twitter')[0];
          this.whatsapp = contacts.filter(contact => contact.name == 'Whatsapp')[0];
        
        } , err => console.log(err));
    }
  // in app browser handling

  browse(url : string) {
    let options : InAppBrowserOptions = {
      location : 'yes' 
    }
    let browser = this.iab.create(url , '_blank' , options);

     browser.show();
  }


   //toast handling

  presentToast(msg : string ){
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }


  
}
