import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Card} from '../../shared/card';
import {Filter} from '../../shared/filter';



@Injectable()
export class DataProvider {

	baseURL : string = 'http://alaman-alteby.com/api/v1/';

  constructor(public http: HttpClient) {
    //console.log('Hello DataProvider Provider');
  }

  getRegions() {
  return this.http.get(this.baseURL + 'region');

  }

//clinics

  getAllClinics(){
    return this.http.get(this.baseURL + `clinics?limit=1000`);
  }
  
  getClinics(filter : Filter , limit : number) {
    
  	return this.http.get(this.baseURL + `clinics?region_id=${filter.regionId}&&city_id=${filter.cityId}&&limit=${limit}`);
  }


//offers  

  getOffers(filter : Filter , limit : number) {
  	return this.http.get(this.baseURL + `offer?region_id=${filter.regionId}&&city_id=${filter.cityId}&&limit=${limit}`);
  }

//notifications
  getNotifications(limit : number) {
  	return this.http.get(this.baseURL + `notifications?limit=${limit}`)
  }

//order card

  addCard(card : Card) {
    var promise = new Promise((resolve , reject) => {
      this.http.post(this.baseURL+ 'card/add', JSON.stringify(card))
        .subscribe(res => {
          resolve(res);
        } , err => {
          reject(err);
        })
    })
  	return promise;
  }

//contact info
	getContact(){
		return this.http.get(this.baseURL + 'allsetting')
	}  
}
