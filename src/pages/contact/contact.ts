import { Component , ViewChild  , ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams  , ToastController , ViewController , Platform } from 'ionic-angular';
import { InAppBrowserOptions ,  InAppBrowser  } from '@ionic-native/in-app-browser';
declare var google : any;
import  {DataProvider} from '../../providers/data/data';
import {Social} from '../../shared/social';


/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
	@ViewChild('map') mapRef : ElementRef;

	segment : string = 'contact';
  canBack : boolean = false;
   //contact
  phone : Social;
  facebook : Social;
  youtube : Social;
  instagram : Social;
  twitter : Social;
  snapchat : Social;
  whatsapp : Social;

  constructor(public navCtrl: NavController, public navParams: NavParams , private toastCtrl : ToastController ,
   private iab : InAppBrowser , private viewCtrl : ViewController , private plt : Platform , 
   private dataService : DataProvider) {
  }

  ionViewWillLoad() {

    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }


    //check for back
    this.canBack = this.viewCtrl.enableBack();

    //get Contact Info
    this.getContact();

    //map
    this.initializeMap();

  }

 
  //for search page

  open(page : string) {
    this.navCtrl.push(page);
  }

  //map handling

  initializeMap() {
  	
  	let LatLng = new google.maps.LatLng(24.6319692 , 46.7150648);
  	
  	let options = {
  		center : LatLng , 
  		zoom : 11
  	}

  	let map = new google.maps.Map(this.mapRef.nativeElement , options);

  	let marker = new google.maps.Marker({
            map: map,
            position: LatLng 
        });

  }

   // in app browser handling

  browse(url : string) {
    let options : InAppBrowserOptions = {
      location : 'yes'
    }

    let browser = this.iab.create(url , '_blank' , options);

    browser.show();

  }


  //toast handling

  presentToast(msg : string ){
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }


  //contact Info

   getContact() {
      this.dataService.getContact()
        .subscribe(res => {
        
          let contacts : Social[] = res['data'];
         
          this.phone = contacts.filter(contact => contact.name == 'phone')[0];
          this.facebook = contacts.filter(contact => contact.name == 'Facebook')[0];
          this.youtube = contacts.filter(contact => contact.name == 'YouTube')[0];
          this.instagram = contacts.filter(contact => contact.name == 'Instagram')[0];
          this.snapchat = contacts.filter(contact => contact.name == 'SnapChat')[0];
          this.twitter = contacts.filter(contact => contact.name == 'Twitter')[0];
          this.whatsapp = contacts.filter(contact => contact.name == 'Whatsapp')[0];
        
        } , err => console.log(err));
    }


 
}
