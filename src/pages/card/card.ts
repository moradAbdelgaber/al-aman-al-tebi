import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController ,
 ToastController , ViewController , Platform } from 'ionic-angular';
import {FormGroup , FormBuilder , Validators} from '@angular/forms';
import {Card} from '../../shared/card';
import {DataProvider} from '../../providers/data/data'

/**
 * Generated class for the CardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-card',
  templateUrl: 'card.html',
})
export class CardPage {

	form : FormGroup;
	formErrors = {
		'name': '' ,
		'age' : '' ,
		'nationality' : '',
    'phone' : '' 
	};

	validationMessages = {
		'name': {
			'required' : 'هذا الحقل مطلوب '
		} ,
		'age' : {
			'required' : 'هذا الحقل مطلوب '
		} ,
		'nationality' : {
			'required' : 'هذا الحقل مطلوب '
		},
    'phone' : {
      'required' : 'هذا الحقل مطلوب '
    }
	}


   canBack : boolean = false;




  constructor(public navCtrl: NavController, public navParams: NavParams , private fb : FormBuilder 
  	, private loadingCtrl : LoadingController , private toastCtrl : ToastController , 
    private dataService : DataProvider ,private viewCtrl : ViewController , private plt : Platform) {
 
  	this.createForm();

  }

  ionViewWillLoad() {
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }

    this.canBack = this.viewCtrl.enableBack();
  }

 
  open(page : string) {
  	this.navCtrl.push(page);
  }

  createForm() {
  	this.form = this.fb.group({
  		name : ['' , Validators.required],
  		age : ['' , Validators.required],
  		nationality : ['' , Validators.required],
      phone : ['' , Validators.required],
      note : ''
  	});

  	this.form.valueChanges.subscribe(data => {
  		this.onValueChanged(data);
  	});

  	this.onValueChanged();  //reset validation messages

  }

  //form validation handling

  onValueChanged(data? : any) {

  	if (!this.form) return;

  	for (var field in this.formErrors) {
  		let control = this.form.get(field);
  		//clear previous messages
  		this.formErrors[field] = '';
  		if (control && control.dirty && !control.valid) {
  			let messages = this.validationMessages[field];
  			for (var key in control.errors) {
  				this.formErrors[field] += messages[key] + ' '
  			}
  		}
  	}

  }
// form submit

  onSubmit() {
  	let loading = this.loadingCtrl.create({
  		content : 'الرجاء الانتظار'
  	});

  	loading.present();

  	let form : Card = this.form.value;

    this.dataService.addCard(form)
      .then(() => {
        loading.dismiss();
        this.presentToast('تم ارسال طلبك بنجاح');
      })
      .catch(err => {
        loading.dismiss();
        this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
      })
      
  }

  //toast handling

  presentToast(msg : string ){
  	let toast = this.toastCtrl.create({
  		message : msg ,
  		duration : 5000 ,
  		cssClass : 'text-center'
  	});

  	toast.present();
  }

}
