import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController , ViewController , Platform} from 'ionic-angular';
import {DataProvider} from '../../providers/data/data';
import {Notification} from '../../shared/notification'

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  limit : number = 20 ;
  notifications : Notification[];
  canBack : boolean = false;


  constructor(public navCtrl: NavController, public navParams: NavParams 
    , private dataService : DataProvider , private toastCtrl : ToastController
    ,private viewCtrl : ViewController , private plt : Platform) {
  }

  ionViewWillLoad() {
    //check for back
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }

    this.canBack = this.viewCtrl.enableBack();


   //get notifications
   this.dataService.getNotifications(this.limit)
     .subscribe(res => {
       this.notifications = res['data'];
     } , err => {
       console.log(err);
       this.notifications = [];
       this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
     })
  }

  //toast handling
 
 presentToast(msg : string) {
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }

  //infinite scroll handling
  getMore(infinitScroll) {
    let newLimit = this.limit + 20;
    this.dataService.getNotifications(newLimit)
     .subscribe(res => {
       infinitScroll.complete();
       this.notifications = res['data'];
     } , err => {
       console.log(err);
       infinitScroll.complete();
       this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
     })
  }

  //for search page
  open(page : string) {
  	this.navCtrl.push(page);
  }

}
