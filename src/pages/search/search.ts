import { Component , ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , Platform  , ViewController , Slides} from 'ionic-angular';
import { Clinic } from '../../shared/clinic';
import { DataProvider } from '../../providers/data/data';
import { Region } from '../../shared/region';
import { City } from '../../shared/city';
import { Filter } from '../../shared/filter';
import { CallNumber } from '@ionic-native/call-number'

/** 
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild(Slides) slides: Slides;

  selectedIndex : number = 0;
	clinics : Clinic[] = [];
	filteredClinics : Clinic[] = [];
  regions : Region[];
  selectedRegion : Region;
  cities : City[];
  selectedCity : City;
  filterOptions : Filter;
  searchValue  : any;
  loading : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams ,
   private dataService : DataProvider , private callNumber : CallNumber , 
   private plt : Platform , private viewCtrl : ViewController) {
  }

  ionViewWillLoad() {

    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }


    //get Regions

    this.dataService.getRegions()
      .subscribe(res => {
        this.regions = res['data'];
       // console.log(this.regions);
        this.selectRegion(false , 0 ,this.regions[0]);
      } , err => {
        console.log(err);
        
      });


    //get all clinics to filter client side
    this.dataService.getAllClinics()
    	.subscribe(res => {
    		this.clinics = res['data'];     
        console.log(this.clinics)   
    	} , err => console.log(err));

  }

  //for search page
   open(page : string) {
    this.navCtrl.push(page);
  }

  //search handling
  getClinics(ev) {
  	
    const val = ev.target.value;

    //store the searchValue global variable
    //to help filter clinics if the user change the region or the city without changing the text
    this.searchValue = ev;

    this.filteredClinics = [];

    let filtered;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
    // console.log(this.filterOptions);
     filtered = this.clinics.filter((clinic) => {
        return (clinic.region_id == this.filterOptions.regionId && 
                clinic.city_id == this.filterOptions.cityId &&
                clinic.name.indexOf(val) > -1);
      });

    // console.log('filtered');
    // console.log(filtered);

     //only show top 20 matching clinics

     for (var i=0;i <= 20;i++) {
     	if (filtered[i] != undefined) {
     		this.filteredClinics.push(filtered[i])
     	}
     }

     this.loading = false;
    // console.log(this.filteredClinics);

    }
  }


  //open clinic detail
 openClinic(clinic : Clinic) {
   this.navCtrl.push('CenterDetailPage' , {
     clinic : clinic
   });
 }

 //for slider

selectRegion(loading : boolean , index : number ,  Region : Region) {
   
    if (loading) {
      this.loading = true
    }
   
   if (index > this.selectedIndex) {
      this.slides.slideNext();
    } 
    else if (index < this.selectedIndex) {
      this.slides.slidePrev();
    }

   this.filteredClinics = null; 
   this.selectedIndex = index;
   this.selectedRegion = Region;
  // console.log(this.selectedRegion);
   this.cities = this.selectedRegion.city;
   this.selectedCity = this.cities[0];
   this.filterOptions = {
     regionId : Region.id ,
     cityId : this.selectedCity.id
   }
   
   if (this.searchValue) {
      this.getClinics(this.searchValue);
   }
  

}
 


 

  selectCity(city : City) {
   
    this.filteredClinics = null;
    this.loading = true;
    this.selectedCity = city;
    this.filterOptions = {
     regionId : this.selectedRegion.id ,
     cityId : this.selectedCity.id
    }
   
    if (this.searchValue) {
        this.getClinics(this.searchValue);
     } else {
       this.loading = false;
     }
  }



 //call clinic
  call(phoneNumber : string){
    this.callNumber.callNumber(phoneNumber , true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));

  }

//tags to json
  tagsToJson(tags) {
    return JSON.parse(tags);
  }


}
