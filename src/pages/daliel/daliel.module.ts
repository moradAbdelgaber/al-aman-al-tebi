import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DalielPage } from './daliel';

@NgModule({
  declarations: [
    DalielPage,
  ],
  imports: [
    IonicPageModule.forChild(DalielPage),
  ],
})
export class DalielPageModule {}
