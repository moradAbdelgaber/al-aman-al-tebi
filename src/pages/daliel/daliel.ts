import { Component , ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController , ViewController , Platform , Slides} from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import {Clinic} from '../../shared/clinic';
import {Region} from '../../shared/region';
import {City} from '../../shared/city';
import {Filter} from '../../shared/filter';
import { CallNumber } from '@ionic-native/call-number';

 

/**
 * Generated class for the DalielPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daliel',
  templateUrl: 'daliel.html',
})
export class DalielPage {
  @ViewChild(Slides) slides: Slides;

	selectedIndex : number = 0;
	regions : Region[];
  selectedRegion : Region;
  cities : City[];
	selectedCity : City;
  clinics : Clinic[];
  limit : number = 20;
  loading : boolean = false;
  filterOptions : Filter;
  canBack : boolean = false;


  constructor(public navCtrl: NavController, public navParams: NavParams
   , private dataService : DataProvider , private toastCtrl : ToastController , 
   private callNumber : CallNumber, private viewCtrl : ViewController , 
   private plt : Platform) {
  }

  ionViewWillLoad() {
    //check for back
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }

    this.canBack = this.viewCtrl.enableBack();
    
    //getRegions
    this.dataService.getRegions()
      .subscribe(res => {
        this.regions = res['data'];
      //  console.log(this.regions);
        this.selectRegion( 0 ,this.regions[0]);
      } , err => {
        console.log(err);
        this.loading = false;
        this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
      });

  }
  

  // for infinite scroll
 
  getMore(infiniteScroll) {

    let newLimit = this.limit + 20;
    
    this.dataService.getClinics(this.filterOptions , newLimit)
     .subscribe(res => {
       infiniteScroll.complete();
       this.clinics = res['data'];
     } , err => {
       console.log(err);
       infiniteScroll.complete();
       this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
     });
   
 }

 //open clinic detail
 openClinic(clinic : Clinic) {
   this.navCtrl.push('CenterDetailPage' , {
     clinic : clinic
   });
 }

  //toast handling

  presentToast(msg : string) {
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }

//for search page
  open(page : string) {
    this.navCtrl.push(page);
  }

//for slider

selectRegion(index : number ,  Region : Region) {
   
   this.loading = true;
   
   if (index > this.selectedIndex) {
      this.slides.slideNext();
    } 
    else if (index < this.selectedIndex) {
      this.slides.slidePrev();
    }

   this.selectedIndex = index;
   this.selectedRegion = Region;
  // console.log(this.selectedRegion);
   this.cities = this.selectedRegion.city;
   this.selectedCity = this.cities[0];
   this.filterOptions = {
     regionId : Region.id ,
     cityId : this.selectedCity.id
   }
   this.limit = 20; 
   this.getClinics(this.filterOptions);

}
 

  search(city : City) {
    this.loading = true;
    this.selectedCity = city;
    this.filterOptions = {
     regionId : this.selectedRegion.id ,
     cityId : this.selectedCity.id
    }
   this.limit = 20; 
   this.getClinics(this.filterOptions);
    
  }

  //get clinics

 getClinics(options : Filter) {

  // console.log('options');
 //  console.log(options);

  this.dataService.getClinics(this.filterOptions ,this.limit)
     .subscribe(res => {
       this.loading = false;
       this.clinics = res['data'];  
     //  console.log(this.clinics);
     } , err => {
       console.log(err);
       this.loading = false;
       this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
     })
  }

  //call clinic
  call(phoneNumber : string){
    this.callNumber.callNumber(phoneNumber , true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));

  }

  //tags to json
  tagsToJson(tags) {
    return JSON.parse(tags);
  }


}
