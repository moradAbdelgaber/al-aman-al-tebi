import { Component , ViewChild  , ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController ,
 Platform , ViewController } from 'ionic-angular';
declare var google : any;
import {Clinic}  from '../../shared/clinic';
import { CallNumber } from '@ionic-native/call-number';




@IonicPage()
@Component({
  selector: 'page-center-detail',
  templateUrl: 'center-detail.html',
})
export class CenterDetailPage {

 @ViewChild('map') mapRef : ElementRef;

	segment : string = 'info'
	clinic : Clinic;

  constructor(public navCtrl: NavController, public navParams: NavParams , private viewCtrl : ViewController ,
   private callNumber : CallNumber ,private toastCtrl : ToastController , private plt : Platform) {

  }

  ionViewDidLoad() {
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }
    this.clinic = this.navParams.get('clinic');
    console.log(this.clinic);
    this.initializeMap(this.clinic.lat , this.clinic.lng);
  }

  

  //search page

  open(page : string) {
    this.navCtrl.setRoot(page);
  }

  //map
  initializeMap(lat : number , lng : number) {
  	
  	let LatLng = new google.maps.LatLng(lat , lng);
  	
  	let options = {
  		center : LatLng , 
  		zoom : 11
  	}

  	let map = new google.maps.Map(this.mapRef.nativeElement , options);

  	let marker = new google.maps.Marker({
            map: map,
            position: LatLng 
        });

  }

  //reinitialize map as it is disappear when you leave the tap and return to it .
  //and also net some time to recognize map div.
  restartMap() {
    setTimeout(() => {
      this.initializeMap(this.clinic.lat , this.clinic.lng);
    } , 1000)
  }

  //call clinic

  call(phone : string) {
   
    this.callNumber.callNumber(phone , true)
      .then(() => {
        console.log('calling');
      })
      .catch(err => {
        console.log(err);
      })
  }

  //toast handling

  presentToast(msg : string ){
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }

  //tags to json
  tagsToJson(tags) {
    return JSON.parse(tags);
  }


}
