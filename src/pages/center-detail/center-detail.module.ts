import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CenterDetailPage } from './center-detail';

@NgModule({
  declarations: [
    CenterDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CenterDetailPage),
  ],
})
export class CenterDetailPageModule {}
