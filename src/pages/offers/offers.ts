import { Component ,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController  ,  ViewController , Platform , Slides} from 'ionic-angular';
import {DataProvider} from '../../providers/data/data';
import {Offer} from '../../shared/offer';
import {Region} from '../../shared/region';
import {City} from '../../shared/city';
import {Filter} from '../../shared/filter';


/**
 * Generated class for the OffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {
 @ViewChild(Slides) slides: Slides;

selectedIndex : number = 0;
segment : string = 'center';
offers : Offer[];
limit : number = 20;
regions : Region[];
selectedRegion : Region;
cities : City[];
selectedCity : City;
loading : boolean = false;
filterOptions : Filter;
canBack : boolean = false;


  constructor(public navCtrl: NavController, public navParams: NavParams , private dataService : DataProvider ,
    private toastCtrl : ToastController ,private viewCtrl : ViewController , 
    private plt : Platform) {
  }

  ionViewWillLoad() {

    //check for back
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }

    this.canBack = this.viewCtrl.enableBack();

     //getRegions
    this.dataService.getRegions()
      .subscribe(res => {
        this.regions = res['data'];
        //console.log(this.regions);
        this.selectRegion( 0 ,this.regions[0]);
      } , err => {
        console.log(err);
        this.loading = false;
        this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
      });
  }

  //infinite scroll handling
 

   getMore(infiniteScroll) {

    let newLimit = this.limit + 20;
    
    this.dataService.getOffers(this.filterOptions , newLimit)
     .subscribe(res => {
       infiniteScroll.complete();
       this.offers = res['data'];
     } , err => {
       //console.log(err);
       infiniteScroll.complete();
       this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
     });
   
 }

  //toast handling

 presentToast(msg : string) {
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }


  //for search page
   open(page : string) {
    this.navCtrl.push(page);
  }

  //for slider

selectRegion(index : number ,  Region : Region) {
   
   this.loading = true;
   
   if (index > this.selectedIndex) {
      this.slides.slideNext();
    } 
    else if (index < this.selectedIndex) {
      this.slides.slidePrev();
    }

   this.selectedIndex = index;
   this.selectedRegion = Region;
  // console.log(this.selectedRegion);
   this.cities = this.selectedRegion.city;
   this.selectedCity = this.cities[0];
   this.filterOptions = {
     regionId : Region.id ,
     cityId : this.selectedCity.id
   }
   this.limit = 20; 
   this.getOffers(this.filterOptions);

}
 
  

  search(city : City) {
    this.loading = true;
    this.selectedCity = city;
    this.filterOptions = {
     regionId : this.selectedRegion.id ,
     cityId : this.selectedCity.id
    }
   this.limit = 20; 
   this.getOffers(this.filterOptions);
    
  }

  //get offers

 getOffers(options : Filter) {

  // console.log('options');
  // console.log(options);

  this.dataService.getOffers(this.filterOptions ,this.limit)
     .subscribe(res => {
       this.loading = false;
       this.offers = res['data'];
     } , err => {
       console.log(err);
       this.loading = false;
       this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
     })
  }


  // open offer details
  openOffer(offer : Offer) {
    this.navCtrl.push('OfferDetailPage' , {
      offer : offer
    });
  }



}
