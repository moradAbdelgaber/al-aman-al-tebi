import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, ToastController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Social } from '../../shared/social';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  canBack : boolean = false;
  about : Social;
  loading : boolean;
  err : boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams , private plt : Platform ,
   private viewCtrl : ViewController , private dataService : DataProvider , private toastCtrl : ToastController) {
  }

  ionViewWillLoad() {
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }

    this.canBack = this.viewCtrl.enableBack();

    //get about section
    this.getAbout()

  }

   //contact Info

   getAbout() {
     this.loading = true;
      this.dataService.getContact()
        .subscribe(res => {
          this.err = false;
          this.loading = false;
          let contacts : Social[] = res['data'];
         
          this.about = contacts.filter(contact => contact.name == 'about')[0];
         
        } , err => {
          this.loading = false;
          this.err = true;
          this.presentToast('حدث خطأ ما . يرجى اعادة المحاولة');
        });
    }

  open(page : string) {
  	this.navCtrl.push(page);
  }

   //toast handling

   presentToast(msg : string) {
    let toast = this.toastCtrl.create({
      message : msg ,
      duration : 5000 ,
      cssClass : 'text-center'
    });

    toast.present();
  }



}
