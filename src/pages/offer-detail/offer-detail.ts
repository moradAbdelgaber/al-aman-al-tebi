import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController , Platform} from 'ionic-angular';
import {Offer} from '../../shared/offer';

/**
 * Generated class for the OfferDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offer-detail',
  templateUrl: 'offer-detail.html',
})
export class OfferDetailPage {

	offer : Offer;

  constructor(public navCtrl: NavController, public navParams: NavParams , 
    private viewCtrl : ViewController , private plt : Platform) {

  	this.offer = this.navParams.get('offer');
  	
  }

  ionViewWillLoad() {
    if (this.plt.is('ios') ) {
      this.viewCtrl.setBackButtonText('رجوع');
    }

  }

}
